//main.c: Main program
//This program is a simple shell
//Author: Justin Henn
//Date: 5/9/16
//Assignment: 6
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <signal.h>
#include "makeargv.h"
#include "path_check.h"
#include "fork_proc.h"
#include "get_prompt.h"




int main() {

  char buffer[1024];
  char delim[] = " \t\n", path_delim[] = ":";
  int i, num_tokens, path_tokens, path_flag;
  char ** myargv;
  char ** pathargv;
  const char * s = getenv("PS1");
  char * created_prompt = malloc( sizeof(char) * MAX_CANON * 4 );
  const char * path;
  char full_path[1024];

  // get path
  if ((path = getenv("MYPATH")) == NULL)  
    path = getenv("PATH");
    
  //separate path into tokens
  if ((path_tokens = makeargv(path, path_delim, &pathargv)) == -1) {

    printf(" Failed ");
    return ( 1 ); 
  }

  //start shell loop
    
  while (1) {

    //get prompt
    if (s != NULL) {

      get_prompt(s, created_prompt);
      printf("%s", created_prompt); 
      strcpy(created_prompt, "\0");
    }
 
    else
      printf("$ "); 

    fflush(NULL);

    //read input
    
    if (fgets(buffer, 1024, stdin)  == NULL) {

      printf("\n");
      break;
      
    }

    //control D or just enter key
    /*if (strcmp(buffer, "\0") == 0)
	break;*/
    if (strcmp(buffer, "exit\n") == 0 || strcmp(buffer, "logout\n") == 0)
      break;
    if (strcmp(buffer, "\n") == 0)
      continue;

    //separate input
  
    if ((num_tokens = makeargv(buffer, delim, &myargv)) == -1) {

      printf(" Failed ");
      return ( 1 ); 
    }

    //check to find command

    path_flag = path_check(path_tokens, pathargv, myargv);
   
    if (path_flag == -1) {

      printf("%s Command not found\n", myargv[0]);
      continue;
    }

    strcpy(full_path, pathargv[path_flag]);
    strcat(full_path, "/");
    strcat(full_path, myargv[0]);

    //check to see if cd or if should fork
    if (strcmp(myargv[0], "cd") == 0)  {
      if (chdir(myargv[1]) != 0)
	printf("Could not change directory\n");
    }

    else {
      fork_proc(full_path, myargv);

    /*  if ( sigprocmask ( SIG_BLOCK, &sig_mask, NULL ) == -1 )  
	return(-1);*/

    }
  }

  free(created_prompt);
  return 0;
}
