//makeargv.c:Splits input given to it
//This program splits the input of char by tokens. This code was provided by the teacher
//Author: Justin Henn
//Date: 5/9/16
//Assignment: 6
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>


int makeargv(const char *s, char *delimiters, char *** argvp) {

    int         error;
    int         i;
    int         numtokens;
    const char * snew;
    char       * t;

    if ( ! ( s && delimiters && argvp ) )
    {
        errno = EINVAL;
        return ( -1 );
    }

    *argvp = NULL;
    snew = s + strspn ( s, delimiters );

    t = ( char * ) malloc ( strlen ( snew ) + 1 );
    if ( !t )
    {
        fprintf ( stderr, "Error allocating memory\n" );
        return ( -1 );
    }

    strcpy ( t, snew );
    numtokens = 0;

    if ( strtok ( t, delimiters ) )
        for ( numtokens = 1; strtok ( NULL, delimiters ); numtokens++ );

    *argvp = ( char ** ) malloc ( ( numtokens + 1 ) * sizeof ( char * ) );
    error = errno;
    if ( ! *argvp )
    {
        free ( t );
        errno = error;
        return ( -1 );
    }

    if ( ! numtokens )
        free ( t );
    else
    {
        strcpy ( t, snew );
        **argvp = strtok ( t, delimiters );
        for ( i = 1; i < numtokens; i++ )
        {
            *((*argvp) + i ) = strtok ( NULL, delimiters );
        }
    }
    *(( *argvp ) + numtokens ) = NULL;
    return ( numtokens );

 }
