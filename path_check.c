//path_check.c: checks path to make sure it is correct
//This program verifies the command that you want to run is available at the path and executable
//Author: Justin Henn
//Date: 5/9/16
//Assignemnt: 6
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <memory.h>

int path_check(int path_tokens, char ** patharg, char ** myarg) {

  int x, path_flag = -1;

  for (x = 0; x < path_tokens; x++) {
 //creates the path 
  char full_path[1024];
    strcpy(full_path, patharg[x]);
    strcat(full_path, "/");
    strcat(full_path, myarg[0]);
//checks to make sure a file is there
    if (access(full_path, F_OK) == 0) {
//checks to make sure you have execute
      if (access(full_path, X_OK) == 0) {

	path_flag = x;
	return path_flag;
      }
    }
  }

  return path_flag;




}

