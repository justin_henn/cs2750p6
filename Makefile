CC	= gcc		# The C compiler
CFLAGS	= -g		# Enable debugging by default
TARGET	= mysh
OBJS	= main.o makeargv.o path_check.o fork_proc.o get_prompt.o

$(TARGET): $(OBJS)
	$(CC) -o $(TARGET) $(OBJS)

main.o:	main.c makeargv.h
	$(CC) $(CFLAGS) -c main.c

makeargv.o: makeargv.c makeargv.h
	$(CC) $(CFLAGS) -c makeargv.c

path_check.o: path_check.c path_check.h
	$(CC) $(CFLAGS) -c path_check.c

fork_proc.o: fork_proc.c
	$(CC) $(CFLAGS) -c fork_proc.c

get_prompt.o: get_prompt.c
	$(CC) $(CFLAGS) -c get_prompt.c

clean:
	/bin/rm -f *.o *~ $(TARGET)
