//fork_proc.h: h file for fork_proc
//This is the include file for fork_proc.c
//Author: Justin Henn
//Date: 5/9/16
//Assignment: 6
#ifndef FORK_H
#define FORK_H

void fork_proc(char *, char **);
//extern pid_t child_pid;
#endif
