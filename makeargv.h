//makeargv.h: H file for makeargv
//This file is an include file for makeargv.c
//Author: Justin Henn
//Date: 5/9/16
//Assignment: 6
#ifndef MAKEARG_H
#define MAKEARG_H

int makeargv(const char *, const char *, char ***);

#endif
