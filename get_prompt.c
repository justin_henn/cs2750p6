//get_prompt.c: get the prompt for the shell
//This program parses the prompt for the shell
//Author: Justin Henn
//Date: 5/9/16
//Assignment: 6
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <unistd.h>
#include "makeargv.h"

void get_prompt(const char * s, char * created_prompt) {


  int x, dir_tokens, add_space = 0;
  char dir_holder[PATH_MAX];
  char * new_dir;
  char ** dir_array;

  //loop that looks through PS1 and creates the prompt
  for (x = 0; x < strlen(s); x++) {

    if(s[x] == '\\') {

      switch(s[x+1]) {

	case 'u':
	  strcat(created_prompt, getenv("USER"));
	  add_space = 1;
	  x++;
	  break;
        case 'h':                          
	  strcat(created_prompt, getenv("HOSTNAME"));
	  add_space = 1;
	  x++;
	  break;
	case 'w':
	case 'W':	
	  new_dir = getcwd(dir_holder, PATH_MAX);
	  if((dir_tokens = makeargv(new_dir, "/", &dir_array)) == -1) {

	    printf("Fail to make prompt");
	    return;
	  }
	  strcat(created_prompt, dir_array[dir_tokens - 1]);
	  add_space = 1;
	  x++;
	  break;		
	case 'n':	
	  strcat(created_prompt, "\n");	
	  add_space = 1;
	  x++;
	  break;
      }
      if (add_space == 1)
	strcat(created_prompt, " ");

      add_space = 0;
    }
  }
  strcat(created_prompt, "$ ");
}
