//get_prompt.h: h file for get_prompt
//This is the include file for get_prompt.c
//Author: Justin Henn
//Date: 5/9/16
//Assignment: 6
#ifndef GET_H
#define GET_H

void get_prompt(const char *, char *);


#endif
