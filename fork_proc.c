//fork_proc.c: fork a process and run a command
//This program does a fork then runs a command in the child
//Author: Justin Henn
//Date: 5/9/16
//Assignment: 6

#include <stdio.h>
#include <sys/types.h>
#include <signal.h>

void fork_proc(char * full_path, char ** myargv) {

  pid_t child_pid;

  int status;

  //fork
  child_pid = fork();

  //checks to see if fork worked
  if (child_pid < 0) {

    printf("Could not fork!");
    return;

  }

  //run command if worked

  else if (child_pid == 0) {

    if (execv(full_path, myargv) < 0) {

      printf("failed");
      return;
    }
  }
  else {

    while (wait(&status) != child_pid)
      ;
  }
}

